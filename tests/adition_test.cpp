#include <gtest/gtest.h>

TEST(Adition, UInt)
{
    EXPECT_EQ(8 + 8, 16);
}

TEST(Adition, Int)
{
    EXPECT_EQ(4 + (-6), -2);
}

TEST(Adition, Float)
{
    EXPECT_FLOAT_EQ(4.5234 + 6.5234, 11.0468);
}